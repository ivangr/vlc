#include <bitio.h>
#include <decode.h>
#include <encode.h>

#include <LiquidCrystal.h>

#include <Event.h>
#include <Timer.h>


int sensorPin = A0;    // select the input pin for the potentiometer
int ledPin = 13;      // select the pin for the LED
int sensorValue = 0;  // variable to store the value coming from the sensor
Timer vlcTimer; // timer for new value;

vlc::decoder4 dec;
vlc::bitwriter writer;

const size_t resultSize = 128;
char result[resultSize];

void setup() {
  Serial.begin(9600);
  // declare the ledPin as an OUTPUT:
  pinMode(ledPin, OUTPUT);  
  vlcTimer.every(1, happiness);
  restart();
  
}

void restart()
{
  writer = vlc::bitwriter(reinterpret_cast<uint8_t*>(result), 8 * sizeof(result), 0);
  dec.restart(&writer);  
}

void loop() {
  vlcTimer.update();
}

void happiness() {
  digitalWrite(ledPin, !digitalRead(ledPin));
  uint16_t value = analogRead(sensorPin);
  dec.feed(value);
  
  if (dec.get_idle_count() > 4 /* frames */) {
    if (dec.is_handshake_done()) {  
      // something was received
      size_t rxByteCount = dec.get_bit_count() / 8;
      result[rxByteCount] = 0;
      Serial.println(result);
    }
    restart();
  }
}

