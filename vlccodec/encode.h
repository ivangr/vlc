//
//  encode.h
//  vlccodec
//
//  Created by Ivan Grokhotkov on 20/02/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#ifndef vlccodec_encode_h
#define vlccodec_encode_h

#include "bitio.h"

namespace vlc {
    
    
    class encoder4
    {
    public:
        encoder4();
        
        void set_range(uint8_t min, uint8_t max);
        void reset(vlc::bitreader *reader);
        size_t values_left() const;
        uint8_t next();
        
    private:
        uint8_t _levels[4];
        size_t _valuesLeft;
        size_t _pos;
        vlc::bitreader* _reader;
        uint8_t _prev;
    };

    
} // namespace vlc

#endif
