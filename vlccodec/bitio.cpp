//
//  bitio.cpp
//  vlccodec
//
//  Created by Ivan Grokhotkov on 21/02/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#include "bitio.h"


namespace vlc {

	void printbits(const uint8_t *buffer, size_t startAtBit, size_t bitsToPrint, char *dstBuffer, size_t dstBufferSize)
	{
		if (dstBufferSize < 1)
			return;

		if (dstBufferSize == 1)
		{
			*dstBuffer = 0;
			return;
		}

		bitreader br(buffer, startAtBit + bitsToPrint, startAtBit);
		const char * dstBufferEnd = dstBuffer + dstBufferSize - 1;

		while (br.bitsLeft() && dstBuffer != dstBufferEnd)
		{
			*(dstBuffer++) = '0' + br.getBit();
		}

		*dstBuffer = 0;
	}

} // vlc::
