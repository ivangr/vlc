//
//  preamble.h
//  vlccodec
//
//  Created by Ivan Grokhotkov on 15/03/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#ifndef vlccodec_preamble_h
#define vlccodec_preamble_h

namespace vlc {
    
    class preamble4tx
    {
    public:
        
        static const size_t length = 12;
        static const uint8_t* value()
        {
            static const uint8_t p[] = { 3, 0, 3, 0, 3, 0, 3, 0, 2, 1, 3, 0 };
            return p;
        }
        
    };
    
    class preamble4rx
    {
    public:
        
        static const size_t length = 8;
        static const uint8_t* value()
        {
            static const uint8_t p[] = { 3, 0, 3, 0, 2, 1, 3, 0 };
            return p;
        }
        
    };
    
}

#endif
