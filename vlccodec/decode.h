//
//  decode.h
//  vlccodec
//
//  Created by Ivan Grokhotkov on 15/03/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#ifndef vlccodec_decode_h
#define vlccodec_decode_h

#include "bitio.h"
#include "window.h"

namespace vlc {

    
    class decoder4
    {
    public:
        typedef int32_t value_t;
        
        decoder4();
        
        void restart(vlc::bitwriter *out);
        
        void feed(value_t val);
        
        bool is_handshake_done() const;
        size_t get_bit_count() const;
        size_t get_idle_count() const;
        
    private:
        static const int _sampleLevelCount = 4;
        static const size_t _windowSize = 15;
        
        vlc::window<value_t, int32_t> _window;
        bool _needFirstSample;
        
        static const int _premul = 100;
        size_t _time;
        const size_t _dataStableCount;
        size_t _dataStable;
        const value_t _dataStableThreshold;
        size_t _sampleCount;
        
        value_t _agcLow;
        value_t _agcHigh;
        value_t _agcAvg;
        
        size_t _agcSamples;
        static const size_t _agcWindowSize = 4;
        value_t _sampleLevelErrors[_sampleLevelCount];
        
        bool _handshake;
        
        vlc::window<uint8_t, uint8_t> _codeWords;
        
        vlc::bitwriter *_out;
        size_t _decodedCount;
    };
    
    
    
} // namespace vlc


#endif
