//
//  window.h
//  vlccodec
//
//  Created by Ivan Grokhotkov on 16/03/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#ifndef vlccodec_window_h
#define vlccodec_window_h

#include <stddef.h>
#include <stdint.h>

namespace vlc {
    
    template<typename T, typename R>
    class window
    {
    public:
        window(size_t count) :
        _n(count)
        {
            _values = new T[count];
            _next = _values;
        }
        
        ~window()
        {
            delete _values;
        }
        
        void reset(T value)
        {
            for (size_t i = 0; i < _n; ++i)
                _values[i] = value;
            _next = _values;
            _mean = (R) value;
            _sum = _n * (R) value;
        }
        
        void push(T value)
        {
            T old = *_next;
            *_next = value;
            
            _sum += (R) (value - old);
            
            _mean = _sum / _n;
            if (++_next == _values + _n)
                _next = _values;
        }
        
        T last() const
        {
            T* pLast = _next - 1;
            if (pLast < _values)
                pLast += _n;
            return *pLast;
        }
        
        R mean() const
        {
            return _mean;
        }
        
        R disp(R premul) const
        {
            R disp = 0;
            for (size_t i = 0; i < _n; ++i)
            {
                R diff = _mean - (R) _values[i];
                if (diff < 0) diff = - diff;
                disp += diff;
            }
            
            return disp * premul / _sum;
        }
        
        bool match_last(const T* source, size_t count)
        {
            if (count > _n)
                return false;
            
            T* pt = _next - count;
            if (pt < _values)
                pt += _n;
            
            for (size_t i = 0; i < _n; ++i)
            {
                if (*pt != source[i])
                    return false;
                if (++pt == _values + _n)
                    pt = _values;
            }
            
            return true;
        }
        
    private:
        const size_t _n;
        T *_values;
        T *_next;
        R _mean;
        R _sum;
    };
    
} // namespace vlc

#endif
