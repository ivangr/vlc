//
//  bitio.h
//  vlccodec
//
//  Created by Ivan Grokhotkov on 20/02/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#ifndef vlccodec_bitio_h
#define vlccodec_bitio_h

#include <stddef.h>
#include <stdint.h>


namespace vlc {

    namespace detail {
    
        template<typename T>
        class bitio
        {
        public:
            bitio(T buffer, size_t sizeInBits, size_t bitOffset) :
                m_ptr(buffer+bitOffset/8),
                m_bitOffset(bitOffset & 0x7),
                m_bitsLeft(sizeInBits - bitOffset)
            {
            }

            size_t bitsLeft()
            {
                return m_bitsLeft;
            }


        protected:
            T m_ptr;
            uint8_t m_bitOffset;
            size_t  m_bitsLeft;
        };
    }

	class bitreader : public detail::bitio<const uint8_t*>
	{
	public:
        bitreader() : detail::bitio<const uint8_t*>(NULL, 0, 0), m_curByte(0) { }

		bitreader(const uint8_t * buffer, size_t sizeInBits, size_t bitOffset) :
            detail::bitio<const uint8_t*>(buffer, sizeInBits, bitOffset),
            m_curByte((*m_ptr) >> m_bitOffset)
        {
            
        }

        uint8_t getBit()
        {
            if (!m_bitsLeft)
                return 0;

            uint8_t result = m_curByte & 1;
            
            if (--m_bitsLeft)
            {
                if (m_bitOffset == 7)
                {
                    m_bitOffset = 0;
                    m_curByte = *(++m_ptr);
                }
                else
                {
                    ++m_bitOffset;
                    m_curByte >>= 1;
                }
            }
            
            return result;
        }

	protected:
		uint8_t m_curByte;
	};


	class bitwriter : public detail::bitio<uint8_t*>
    {
    public:
        bitwriter() : detail::bitio<uint8_t*>(NULL, 0, 0), m_mask(0) { }

    	bitwriter(uint8_t* buffer, size_t sizeInBits, size_t bitOffset) :
            detail::bitio<uint8_t*>(buffer, sizeInBits, bitOffset),
            m_mask(1 << m_bitOffset)
    	{
    	}

    	void putBit(uint8_t bit)
    	{
    		if (!m_bitsLeft)
    			return;

    		*m_ptr &= ~ m_mask;
    		*m_ptr |= (m_mask & (bit << m_bitOffset));

    		if (m_bitOffset == 7)
    		{
    			m_bitOffset = 0;
    			++m_ptr;
    			m_mask = 1;
    		}
    		else
    		{
    			++m_bitOffset;
    			m_mask <<= 1;
    		}
    	}

    private:
    	uint8_t m_mask;
    };

    // print bits as '0' and '1' to string buffer, in lsb to msb order
    void printbits(const uint8_t *buffer, size_t startAtBit, size_t bitsToPrint, char *dstBuffer, size_t dstBufferSize);

} // namespace vlc

#endif


