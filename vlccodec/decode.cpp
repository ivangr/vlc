//
//  decode.cpp
//  vlccodec
//
//  Created by Ivan Grokhotkov on 15/03/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#define __STDC_LIMIT_MACROS
#include "bitio.h"
#include "decode.h"
#include "preamble.h"

// #define DEBUG_SAMPLING
// #define DEBUG_AGC
// #define DEBUG_CODEWORDS
// #define DEBUG_DECODING

namespace vlc {


    namespace detail {
        template <typename T>
        T min(T a, T b) {
            return (a < b) ? a : b;
        }

        template <typename T>
        T max(T a, T b) {
            return (a > b) ? a : b;
        }

        template <typename T>
        T limit_max();

        template <>
        int16_t limit_max<int16_t>()
        {
            return INT16_MAX;
        }

        template <>
        int32_t limit_max<int32_t>()
        {
            return INT32_MAX;
        }
        
    }
    
    decoder4::decoder4() :
        _window(_windowSize),
        _dataStableThreshold(0.05 * _premul),
        _dataStableCount(_windowSize / 2),
        _out(NULL),
        _codeWords(vlc::preamble4rx::length)
    {
    }
    
    void decoder4::restart(vlc::bitwriter *out)
    {
        _out = out;
        _agcLow = 3000;
        _needFirstSample = true;
        _dataStable = 0;
        _sampleCount = 0;
        _agcSamples = 0;
        _handshake = false;
        _decodedCount = 0;
    }
    
    void decoder4::feed(value_t val)
    {
        bool haveSampled = false;
        
        if (_needFirstSample)
        {
            _needFirstSample = false;
            _window.reset(val);
        }
        
        _window.push(val);
        value_t mean = _window.mean();
        value_t disp = _window.disp(_premul);
        value_t sample = 0;
        uint8_t codeWord = 0;
        uint8_t decoded = 0;
        
        if (disp < _dataStableThreshold)
        {
            if (++_dataStable == _dataStableCount)
            {
                haveSampled = true;
                sample = mean;
#ifdef DEBUG_SAMPLING
                printf("t=%ld sample#=%ld  value=%d\n", _time, _sampleCount, sample);
#endif
                ++_sampleCount;
            }
        }
        else
        {
            _dataStable = 0;
        }

        if (haveSampled)
        {
            if (_agcSamples < _agcWindowSize)
            {
                _agcLow = detail::min(_agcLow, sample);
                _agcHigh = detail::max(_agcHigh, sample);
                _agcAvg = (_agcLow + _agcHigh) / 2;
#ifdef DEBUG_AGC
                printf("t=%ld sample=%d agc %d %d\n", _time, sample, _agcLow, _agcHigh);
#endif
                ++_agcSamples;
            }
            else
            {
                value_t sampleDelta = (_agcHigh - _agcLow) / (_sampleLevelCount - 1);
                value_t minError = detail::limit_max<value_t>();
                size_t minErrorLevel = -1;
                for (size_t iLevel = 0; iLevel < _sampleLevelCount; ++iLevel)
                {
                    value_t error = _agcLow + (value_t)iLevel * sampleDelta - sample;
                    _sampleLevelErrors[iLevel] = error;
                    error = (error < 0) ? -error : error;
                    if (error < minError)
                    {
                        minError = error;
                        minErrorLevel = iLevel;
                    }
                }
                codeWord = minErrorLevel;
                _codeWords.push(codeWord);
                if (minErrorLevel > (_sampleLevelCount - 1)/2)
                    _agcHigh -= _sampleLevelErrors[minErrorLevel] / 2;
                if (minErrorLevel < (_sampleLevelCount - 1)/2)
                    _agcLow -= _sampleLevelErrors[minErrorLevel] / 2;
                _agcAvg = (_agcLow + _agcHigh) / 2;
#ifdef DEBUG_AGC
                printf("t=%ld sample=%d agc %d %d\n", _time, sample, _agcLow, _agcHigh);
#endif
#ifdef DEBUG_CODEWORDS
                printf("t=%ld value=%d codeword=%ld\n", _time, sample, codeWord);
#endif
            }
            
            if (!_handshake)
            {
                if (_sampleCount >= vlc::preamble4rx::length + _agcWindowSize)
                {
                    if (_codeWords.match_last(vlc::preamble4rx::value(), vlc::preamble4rx::length))
                    {
                        _handshake = true;
#ifdef DEBUG_DECODING
                        printf("handshake t=%ld\n", _time);
#endif
                    }
                }
            }
            else
            {
                if (codeWord == 3 || codeWord == 1)
                    decoded = 1;
                else if (codeWord == 0 || codeWord == 2)
                    decoded = 0;
#ifdef DEBUG_DECODING
                printf("t=%ld codeword=%d decoded=%d\n", _time, codeWord, decoded);
#endif
                _out->putBit(decoded);
                ++_decodedCount;
            }
        }

        ++_time;
    }
    
    bool decoder4::is_handshake_done() const
    {
        return _handshake;
    }
    
    size_t decoder4::get_bit_count() const
    {
        return _decodedCount;
    }
    
    size_t decoder4::get_idle_count() const
    {
        return _dataStable / _windowSize;
    }
}
