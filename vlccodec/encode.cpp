//
//  encode.c
//  vlccodec
//
//  Created by Ivan Grokhotkov on 14/03/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#include "bitio.h"
#include "encode.h"
#include "preamble.h"


namespace vlc {
    
    encoder4::encoder4() :
        _valuesLeft(0), _pos(0), _reader(NULL)
    {
        set_range(0, 255);
    }
    
    void encoder4::set_range(uint8_t min, uint8_t max)
    {
        _levels[0] = min;
        _levels[1] = static_cast<uint8_t>((2 * (uint16_t)min + (uint16_t)max)/3);
        _levels[2] = static_cast<uint8_t>(((uint16_t)min + 2 * (uint16_t)max)/3);
        _levels[3] = max;
    }
    
    void encoder4::reset(vlc::bitreader *reader)
    {
        _reader = reader;
        _valuesLeft = 0;
        _pos = 0;
        if (reader)
        {
            _valuesLeft = preamble4tx::length + reader->bitsLeft();
        }
    }
    
    size_t encoder4::values_left() const
    {
        return _valuesLeft;
    }
    
    uint8_t encoder4::next()
    {
        if (_valuesLeft == 0)
        {
            return 0;
        }
        
        uint8_t out;
        if (_pos < vlc::preamble4tx::length)
        {
            out = vlc::preamble4tx::value()[_pos];
        }
        else
        {
            uint8_t bit = _reader->getBit();
            static const uint8_t lut[2][4] = { {2, 0, 0, 0}, {3, 3, 3, 1} };
            out = lut[bit][_prev];
        }
        ++_pos;
        --_valuesLeft;
        _prev = out;
        return _levels[out];
    }
    
} // namespace vlc

