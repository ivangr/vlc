$(function(){


  function string2Bin(s) {
    var b = new Array();
    var last = s.length;

    for (var i = 0; i < last; i++) {
      var d = s.charCodeAt(i);
      if (d < 128)
        b[i] = dec2Bin(d);
      else {
        var c = s.charAt(i);
        console.log(c + ' is NOT an ASCII character');
        b[i] = '';
      }
    }
    return b.join('');
  }

  function dec2Bin(d) {
    var b = '';

    for (var i = 0; i < 8; i++) {
      b = (d%2) + b;
      d = Math.floor(d/2);
    }

    return b;
  };


  function encode4 (bitlist) {
    prev = 1
    var lut = [[2, 0, 0, 0], [3, 3, 3, 1]];
    var result = [3,0,3,0,2,1,3,0];
    for (i in bitlist) {
      var cur = parseInt(bitlist[i]);
      var out = lut[cur][prev]
      result.push(out) 
      prev = out
    }
    result.push(0); 
    return result.join('');
  }

  // def encode4(bitlist):
  // prev = 1
  // result = np.zeros(len(bitlist))
  // lut = [ [2, 0, 0, 0], [3, 3, 3, 1] ]
  // for i in range(0, len(bitlist)):
  //     cur = int(bitlist[i])
  //     out = lut[cur][prev]
  //     result[i] = out
  //     prev = out
  // return result

  // lut = [ [2, 0, 0, 0], [3, 3, 3, 1] ]

  var state = {
      repeater: null,
      msg: '',
      pos: 0,
      stop: false
  };

  drawNext = function() {
    var panel = $('#vlcpanel')
    var c = state.msg[state.pos];
    var color = 255*c/3; 
    panel.css('background-color', "rgb("+color+","+color+","+color+")");
    ++state.pos;
    if (state.pos == state.msg.length)
    {
      state.stop = true;
    }
  }

  drawLoop = function() {
    if (!state.stop)
    {
      state.repeater = window.requestAnimationFrame(drawLoop);
      drawNext();
    }
  }


  $('#start').click(function(){
    state.stop = false;
    state.msg = encode4(string2Bin($('#phrase').val()));
    console.log (state.msg);
    state.pos = 0;
    drawLoop();
  });
  });
