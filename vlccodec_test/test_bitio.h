//
//  test_bitio.h
//  vlccodec
//
//  Created by Ivan Grokhotkov on 21/02/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#ifndef vlccodec_test_bitio_h
#define vlccodec_test_bitio_h

#include "bitio.h"
#include <vector>
#include <cstring>

namespace vlc {

	class test_bitio
	{
	public:
        
        bool test_reader_8bit()
        {
            uint8_t val = 0x51;
            bitreader r(&val, 8, 0);
            return (r.getBit() == 1 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 1 &&
                    r.getBit() == 0 &&
                    r.getBit() == 1 &&
                    r.getBit() == 0 );
        }
        
        bool test_reader_16bit()
        {
            uint16_t val = 0x5101;
            bitreader r(reinterpret_cast<uint8_t*>(&val), 16, 0);
            return (r.getBit() == 1 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    
                    r.getBit() == 1 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 0 &&
                    r.getBit() == 1 &&
                    r.getBit() == 0 &&
                    r.getBit() == 1 &&
                    r.getBit() == 0 );
        }
        
        
		bool test_printer_one(const char *gold, const uint8_t* src, size_t bitStart, size_t bitsToPrint)
		{
			std::vector<char> dst(bitsToPrint + 1, 0);
			printbits(src, bitStart, bitsToPrint, &dst[0], dst.size());
			return strcmp(gold, &dst[0]) == 0;
		}

		bool test_printer_one_i32(const char *gold, int32_t val, size_t bitStart, size_t bitsToPrint)
		{
			return test_printer_one(gold, reinterpret_cast<uint8_t*>(&val), bitStart, bitsToPrint);
		}

		bool test_printer()
		{
			return 	test_printer_one_i32("0", 5, 1, 1) &&
					test_printer_one_i32("1", 5, 0, 1) &&
					test_printer_one_i32("1", 5, 2, 1) &&
					test_printer_one_i32("1010", 5, 0, 4) &&
					true;
		}
	};

} // vlc::

#endif
