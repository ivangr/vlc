//
//  main.cpp
//  vlccodec
//
//  Created by Ivan Grokhotkov on 20/02/14.
//  Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
//

#include <iostream>
#include <fstream>
#include "test_bitio.h"
#include "encode.h"
#include "decode.h"

void test_bitmanip()
{
    vlc::test_bitio test;
    std::cout << test.test_reader_8bit() << std::endl;
    std::cout << test.test_reader_16bit() << std::endl;
    std::cout << test.test_printer() << std::endl;
    
}

void test_enc4()
{
    vlc::encoder4 enc;
    const char msg[] = "test test";
    vlc::bitreader reader(reinterpret_cast<const uint8_t*>(msg), 8 * sizeof(msg), 0);
    enc.set_range(0, 31);
    enc.reset(&reader);
    while(enc.values_left())
    {
        uint8_t value = enc.next();
        for (int j = 0; j < value; ++j)
            std::cout << ' ';
        std::cout << '#' << std::endl;
        
    }
}

void test_dec4(const char* filename)
{
    vlc::decoder4 dec;
    uint8_t result[64];
    vlc::bitwriter writer(result, sizeof(result) * 8, 0);
    dec.restart(&writer);
    std::ifstream fin(filename);
    fin >> std::hex;
//    std::cout << "[ ";
    while(fin.good())
    {
        int32_t value;
        fin >> value;
        dec.feed(value);
    }
    char printed[256];
    vlc::printbits(result, 0, dec.get_bit_count(), printed, sizeof(printed));
    std::cout << printed << std::endl;
//    std::cout << "]" << std::endl;
}

void test_enc_dec()
{
    const char message[] = "/join/HomeWLAN/suP3RpA55word/";
    char receiveBuffer[sizeof(message)+4];
    vlc::bitreader reader((uint8_t*) message, 8 * sizeof(message), 0);
    vlc::bitwriter writer((uint8_t*) receiveBuffer, 8 * sizeof(receiveBuffer), 0);
    vlc::encoder4 enc;
    enc.reset(&reader);
    vlc::decoder4 dec;
    dec.restart(&writer);
    while(enc.values_left())
    {
        int value = (int) enc.next() * 10 + 900;
        // encoder runs at 32 ms period
        // decoder runs at 1 ms period
        for (int t = 0; t < 32; ++t)
        {
            dec.feed(value);
        }
    }
    receiveBuffer[dec.get_bit_count() / 8 + 1] = 0;
    if (strcmp(message, receiveBuffer) != 0)
    {
        std::cout << "expected: " << message << std::endl << "received: " << receiveBuffer;
    }
}

int main(int argc, const char * argv[])
{
//    test_enc_dec();
//    return 0;
    
    if (argc < 2)
    {
        return 1;
    }
    
    test_dec4(argv[1]);
    
    return 0;
}

